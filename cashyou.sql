-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 18, 2020 at 12:07 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cashyou`
--

-- --------------------------------------------------------

--
-- Table structure for table `google_files`
--

CREATE TABLE `google_files` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `download_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_size` int(11) NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `google_files`
--

INSERT INTO `google_files` (`id`, `title`, `download_url`, `file_size`, `mime_type`) VALUES
(1, '#141 API', 'https://www.googleapis.com/drive/v2/files/1OHqpErwrZzW_3i9gAeAZdECbA5Kts8OB1bCv8dGw4E8', 888, 'application/vnd.google-apps.document'),
(2, '#306 API', 'https://www.googleapis.com/drive/v2/files/1jTWxG7obqr1bx5lBeOrOTq5ocDim28SIFDcPRWuNGi8', 888, 'application/vnd.google-apps.document'),
(3, '#81 ON-OFF API', 'https://www.googleapis.com/drive/v2/files/10SFgJ7tHulbLSsBj17cpcPZ40nQFq6ceLJ00jvWy4SI', 888, 'application/vnd.google-apps.document'),
(4, 'Full Stack Developer', 'https://www.googleapis.com/drive/v2/files/1E5RVVMZ4r5C8VcRHLNq4L9cne-BmyGZecwnSdOH0GCs', 888, 'application/vnd.google-apps.document'),
(5, '#322 API', 'https://www.googleapis.com/drive/v2/files/19QsB98ePwERXJ0Fl7I8a2zWRY7sBDzCR3CSE9D_iLxM', 888, 'application/vnd.google-apps.document'),
(6, 'G-Tech API', 'https://www.googleapis.com/drive/v2/files/1fDIA5Egf4hPUKjn6tjt36rzMbGuhxrDxoLp2iasrEvc', 888, 'application/vnd.google-apps.document'),
(7, 'On/Off Android Versions', 'https://www.googleapis.com/drive/v2/files/1A7VXF-jxesqMCTiEbL_OK9_iwg0Mko6Qakcgf_dlwqo', 888, 'application/vnd.google-apps.document'),
(8, 'Nashmi Dashboard', 'https://www.googleapis.com/drive/v2/files/1p0pw36TQ7SSd7x6hagTa-EoEY97nMSJj0g1Bn4dNkRI', 888, 'application/vnd.google-apps.document'),
(9, 'Nashmi API', 'https://www.googleapis.com/drive/v2/files/1Z3djwCM0LrU4UrWSl2AQvvD2eYLe3wF8aOcyudJX8qs', 888, 'application/vnd.google-apps.document'),
(10, '#190 API', 'https://www.googleapis.com/drive/v2/files/1MFyDULtEnk4fTjN1-y4ARb1ZVB6wmtPYVICa4qVnwuw', 888, 'application/vnd.google-apps.document'),
(11, 'Dokhn API', 'https://www.googleapis.com/drive/v2/files/1ll6n5RLfsluRcXoaXcGA99Blp_CuuPSwKa8S0VT0KBw', 888, 'application/vnd.google-apps.document'),
(12, 'coverletter.pdf', 'https://www.googleapis.com/drive/v2/files/1GlqqlYTUAzp0LojeyzIYjUH9wOcyYwjS', 888, 'application/pdf'),
(13, 'CV_last_update_09_2019.pdf', 'https://www.googleapis.com/drive/v2/files/195M0NH9DBN6h3EI-o0B246Ic3Q14dYF7', 888, 'application/pdf'),
(14, 'Amrak API', 'https://www.googleapis.com/drive/v2/files/1mJOI1VMzo1669zPquPaR_rRmFuYEwr-7-AciZWJJP7s', 888, 'application/vnd.google-apps.document'),
(15, 'Leader Api Design', 'https://www.googleapis.com/drive/v2/files/1RtevZ6YONZarxUaFmKsrBdYS2xDe8jv1ezeotQQ4-bY', 888, 'application/vnd.google-apps.document'),
(16, 'Mohamed-Abd-Elrahman.pdf', 'https://www.googleapis.com/drive/v2/files/1-GZVlCFUkRSGnzrdHN5KKjueVO6ptpdD', 888, 'application/pdf'),
(17, '306 API Report', 'https://www.googleapis.com/drive/v2/files/1UOQM_PvOSXW8XPenspoSRJtlNuojj8SU3wrS79RlD5s', 888, 'application/vnd.google-apps.document'),
(18, 'donation.zip', 'https://www.googleapis.com/drive/v2/files/12XF5_BUtXo4RkgI_UsGOx1nzKtH17rD6', 888, 'application/zip'),
(19, 'efinance', 'https://www.googleapis.com/drive/v2/files/13T5xMol7O3juu69y_VXnwZUhMTJWpGHo', 888, 'application/vnd.google-apps.map'),
(20, 'VanHack Resume Template [with tips]', 'https://www.googleapis.com/drive/v2/files/1dosdSqe2n1gOnLRyjTzSajcNlCBZlbrMc3RZQpoFxr4', 888, 'application/vnd.google-apps.document'),
(21, 'Sajjel Api.docx', 'https://www.googleapis.com/drive/v2/files/1t3d_Ant1yyB_V9_-bS4xz-aewQLAJVzeyLjQSnbgRlk', 888, 'application/vnd.google-apps.document'),
(22, '#062 API', 'https://www.googleapis.com/drive/v2/files/1y4mBJ3i_k8ElfNV56Sc3ZJPEXsqTMDu7EMi4uVz-jwk', 888, 'application/vnd.google-apps.document'),
(23, 'G Tech Report', 'https://www.googleapis.com/drive/v2/files/1FBYnXgllVLULrkL0D_K0BKSNxmJ_GDz1VZE2QuSLJr0', 888, 'application/vnd.google-apps.document'),
(24, 'Find Me Api.docx', 'https://www.googleapis.com/drive/v2/files/18zIsoO4Ha5u_Ui8gMseyaMQSuXKnOQlgnOHCVBCXEPk', 888, 'application/vnd.google-apps.document'),
(25, 'Runway', 'https://www.googleapis.com/drive/v2/files/1oif0b0MSYRN_mKvP36DDVq_nEOyBHliZwSLKCSCgu7I', 888, 'application/vnd.google-apps.document'),
(26, 'TMT-Editedlastmmmmmmyyyyyyyyyyyyy.zip', 'https://www.googleapis.com/drive/v2/files/1Fy3ABGjWQfGrznQQnXwvUDK2dRMii-0U', 888, 'application/zip'),
(27, 'TMT-Editedlastmmmmmm.zip', 'https://www.googleapis.com/drive/v2/files/1BLQPb8ZmbhhSwFMDRlwfc59z-1_IC8kj', 888, 'application/zip'),
(28, '#81 on-off Report', 'https://www.googleapis.com/drive/v2/files/11WoMdn7S2IGm9T7F0oeCcGwYAZfGOj_AnQ__m0Y7UdQ', 888, 'application/vnd.google-apps.document'),
(29, 'TMT-Editedlast.zip', 'https://www.googleapis.com/drive/v2/files/1Z8k-sXn6sVyQaWGl2uv0xfB0aZHtrhgO', 888, 'application/zip'),
(30, 'TMTneer.zip', 'https://www.googleapis.com/drive/v2/files/1zBVRbmqnuuapGNyxxDRfnYQeVgegITGY', 888, 'application/zip'),
(31, 'TMTlast.zip', 'https://www.googleapis.com/drive/v2/files/1K0eQgXbufcyw0E2PaysUujc2_fp660bb', 888, 'application/zip'),
(32, '#223 website', 'https://www.googleapis.com/drive/v2/files/1Nyzo5qwX6jhesH6ROoyzyMPH0G3o-08tBIVwPglOOP8', 888, 'application/vnd.google-apps.document'),
(33, '#98 Report', 'https://www.googleapis.com/drive/v2/files/1_b2EWd2tvE3cJKPK8NOhydWBiT2NxWF4SQ4Nzb6fgmA', 888, 'application/vnd.google-apps.document'),
(34, '#98 API', 'https://www.googleapis.com/drive/v2/files/1YEDiJH8Jn-bMS9eI7sve806hJh__IbQO1PUb9eAGNJ8', 888, 'application/vnd.google-apps.document'),
(35, 'Sajjel 3rd Report', 'https://www.googleapis.com/drive/v2/files/1HWMZrKBCQr7wqZ2Ot6oIUnoTJFDQPTJmr-avXkLdSLg', 888, 'application/vnd.google-apps.document'),
(36, 'Sajjel 2nd Report ', 'https://www.googleapis.com/drive/v2/files/1uuQ0oa1TVzrSY0-lX5TzTVRp3HxlZYG27LayNggckPs', 888, 'application/vnd.google-apps.document'),
(37, 'Sajjel', 'https://www.googleapis.com/drive/v2/files/1ok7lKsqRmUug1q1MbOzvLq_vDOsyf5YhRwF6OSGo_i0', 888, 'application/vnd.google-apps.document'),
(38, 'Adobe XD CC 2019-02-16 16-53-36.mp4', 'https://www.googleapis.com/drive/v2/files/1I3V310BHmS4JP5dQwHHV8Y11cGtfF-s7', 888, 'video/mp4'),
(39, 'my CV.docx', 'https://www.googleapis.com/drive/v2/files/1CnJu8E4NPH2OXLABqw1sbwhxky77vaPg', 888, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
(40, 'Leader Notes', 'https://www.googleapis.com/drive/v2/files/1VJfNgIC7TIhqkg780WH-X1EFsfRd2-Bmfcm6iZSfwTU', 888, 'application/vnd.google-apps.document'),
(41, '#98 Dashboard', 'https://www.googleapis.com/drive/v2/files/14O1AVggsp7MUyaVV1O_QTuJ3XWM4AOL14otbOirpK4A', 888, 'application/vnd.google-apps.document'),
(42, '#068 API', 'https://www.googleapis.com/drive/v2/files/1zhYHgACOh9XbekRkdJO5jaezO5QI7fgg6T7HRFd63RM', 888, 'application/vnd.google-apps.document'),
(43, 'Khadamaty Design & Website', 'https://www.googleapis.com/drive/v2/files/1m4hhXXWW4xgecqzJ23wiIj0opeGuIHboqtkQnUHu2bc', 888, 'application/vnd.google-apps.document'),
(44, 'On-Off Dashboard Report', 'https://www.googleapis.com/drive/v2/files/1oZC8mjNIJ2LLmKqS97FS32_D4vkyB5JVnAtxHHHv1-I', 888, 'application/vnd.google-apps.document'),
(45, 'sajjel full test', 'https://www.googleapis.com/drive/v2/files/18P6ST83mA5K9A0hspmurnznnufPLnkElxGNlxIEavHA', 888, 'application/vnd.google-apps.document'),
(46, 'COMPLETE VANHACK PROFILE', 'https://www.googleapis.com/drive/v2/files/1JLhb03tIuPdlAkX6egSqHRAWMkBJ3PStTEbKl8BK-UY', 888, 'application/vnd.google-apps.document'),
(47, 'The start date and end date of university not displayed', 'https://www.googleapis.com/drive/v2/files/1OGZkdWLg8-NJXjrSAWWG-2T5LPRJP780-rqepH1R-ME', 888, 'application/vnd.google-apps.document'),
(48, 'Fostany Dashboard', 'https://www.googleapis.com/drive/v2/files/1fztRZ5VFL0B--Aq19y9TDN6vB--Xnj4tD2Otkw7-lLg', 888, 'application/vnd.google-apps.document'),
(49, 'MagdSoft website', 'https://www.googleapis.com/drive/v2/files/1h_K483_qs10fZqZGWjbuWJHMXLi2MYiqi8NMtf_hp1Q', 888, 'application/vnd.google-apps.document'),
(50, 'Bonus System - V1 ', 'https://www.googleapis.com/drive/v2/files/10pocEW8_q5ypz5-zmdc8O8-aGKnnkTTUuYmHnbkF99s', 888, 'application/vnd.google-apps.document'),
(51, 'cases leader', 'https://www.googleapis.com/drive/v2/files/1tPBnRdVCGn-XUpWFUVyFcHT9DOFZzDmEZrTOwQ2kAhU', 888, 'application/vnd.google-apps.document'),
(52, 'test.php', 'https://www.googleapis.com/drive/v2/files/1vq6chhvtEqdgn-J0Tk9deC50Kdpl9nHqPl3Xpbl_uDqDOx7WPlRG5IVE', 888, 'application/vnd.google-apps.script'),
(53, 'AnswerMe API.docx', 'https://www.googleapis.com/drive/v2/files/1dMCz1zTAZKiYMI0c5igTVH6lTYSDcuU5YTX2T0bVTuI', 888, 'application/vnd.google-apps.document'),
(54, 'مبادرة تأهيل المرأة المصرية لريادة الأعمال.pdf', 'https://www.googleapis.com/drive/v2/files/1URdc7xoGXav8oQn55xZIbWl-oblklpb0', 888, 'application/pdf'),
(55, 'هديه V2.docx', 'https://www.googleapis.com/drive/v2/files/1y9Zexg-W0AaDh53BYN3QuwX4G-cEd7nSDktDc-vAcDU', 888, 'application/vnd.google-apps.document'),
(56, 'Exam_project.rar', 'https://www.googleapis.com/drive/v2/files/1noYOfBOWSSb-mFuXb62-qY4uCkwERQfe', 888, 'application/rar'),
(57, 'MODON v5pdf.pdf', 'https://www.googleapis.com/drive/v2/files/0B30LZL1ar37LYUl4Rk9TRWxmaVk', 888, 'application/pdf'),
(58, 'Untitled Site', 'https://www.googleapis.com/drive/v2/files/1obiQK8us7u2lUpt-l_CkThqBP8XnB8pj', 888, 'application/vnd.google-apps.site'),
(59, 'Mohamed Fathy English CV.docx', 'https://www.googleapis.com/drive/v2/files/0B7N9tDOtjDQjZHBVX0RlYjMtUFE', 888, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'),
(60, 'Professional Loc-v2.pdf', 'https://www.googleapis.com/drive/v2/files/0B5rJ6k1dYVfeRktLdk1sc0dTc2M', 888, 'application/pdf'),
(61, '598', 'https://www.googleapis.com/drive/v2/files/0B9x5L29xpeaaX1dkOVlYV2ljbkk', 888, 'application/pdf'),
(62, 'AdminLTE-RTL.min.css', 'https://www.googleapis.com/drive/v2/files/0B_3yg7XQ6LqyeVdRSmhwSmVsc2M', 888, 'text/css'),
(63, '_2017__2.rar', 'https://www.googleapis.com/drive/v2/files/0B7aQiU7nV3LrYTRWVTdCdUJnak0', 888, 'application/x-rar'),
(64, 'سيرة ذاتية ', 'https://www.googleapis.com/drive/v2/files/1V38aKv26A1Z7n0I7zYWsusEAXyT51xFsd0YqJ5-VJzs', 888, 'application/vnd.google-apps.document'),
(65, 'barmaja.rar', 'https://www.googleapis.com/drive/v2/files/0B2Tk1FVSnQQHOVhEMmI0dVUzWkE', 888, 'application/x-rar'),
(66, 'سيرة ذاتية', 'https://www.googleapis.com/drive/v2/files/0B7N9tDOtjDQjZEdNWld4NHpmNjg', 888, 'application/vnd.google-apps.folder');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `google_files`
--
ALTER TABLE `google_files`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `google_files`
--
ALTER TABLE `google_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
