<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GoogleFile extends Model
{
    protected $table = 'google_files';
    public $timestamps = false;
}
