<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GoogleFile;


class TestController extends Controller
{
    public function CheckAnagrams (Request $request) 
    {
        /// validate the request.
        $validatedData = $request->validate([
            'param_one' => 'required|string',
            'param_two' => 'required|string',
        ]);

        $str_one = $request->param_one;
        $str_two = $request->param_two;

        // convert the string to an array.
        $str_one = str_split($str_one);
        $str_two = str_split($str_two);

        // check for length if they do not match return false
        if (count($str_one) !== count($str_two)) {
            return 'false';
        }

        // sort the array        
        sort($str_one);
        sort($str_two);

        // join elements of an array with a string
        $one = implode('', $str_one); 
        $two = implode('', $str_two); 

        // check match or not 
        // this should actually return true and false as boolean 
        // but false will cast to nothing so replaced by string 'true' , 'false'
        return $one === $two ? 'true' : 'false';
    }


    public function fetch_files () 
    {
        $url = 'https://www.googleapis.com/drive/v2/files?fields=*&key=AIzaSyCP1C3SYNHgTbclO5_hkJjO2XyQPHYqf78';

        $headers = array (
            'Authorization: Bearer ya29.a0Adw1xeWNgfJw5-dX8_Iqw2ePbJsw1P4yF7g_h577SHmwcZb5FjrCRLooq_YQ3b7s1z19w0XvacgwCbFIOaBiDa3EJGCqbaDgcnCXYzk8d6bN6wdcOogHI2u6IuQhsef2n2mU1wdcAoUmIkstIB-JipMthsFFynSDdLc',
            'Accept: application/json',
        );

        // the size only need field=size but this does not work on curl
        // so i have to hardcode it right now.
        $ch = curl_init();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_HTTPGET, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        //curl_setopt ( $ch, CURLOPT_POSTFIELDS, 'fields=size');

        $result = curl_exec ( $ch );
        curl_close ( $ch );
        return $result;
    }

    
    function getting_files () 
    {
        $response = $this->fetch_files();
        $response = json_decode($response, true);
        dd($response);
        $response_files = $response['items'];

        $db_files = GoogleFile::all();
        if (count($db_files) !== count($response_files)) {
            GoogleFile::truncate();

            foreach($response_files as $file) {
                $GoogleFile = new GoogleFile();
                $GoogleFile->title = $file['title'];
                $GoogleFile->download_url = isset($file['downloadUrl']) ? $file['downloadUrl'] : $file['alternateLink'];
                $GoogleFile->file_size = isset($file['fileSize']) ? $file['fileSize'] / 1000 : 0;
                $GoogleFile->mime_type = $file['mimeType'];
                $GoogleFile->save();
            }
        }


        $files = GoogleFile::all();
        return view('task_two', compact('files'));
    }
}
